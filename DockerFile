FROM ubuntu:14.04

ADD . /code
WORKDIR /code

COPY run_commands.sh /entrypoint.sh
COPY update_mobile.sh /update_mobile.sh
COPY update_angularjs_layout.sh /update_angularjs_layout.sh

RUN chmod +x /entrypoint.sh
RUN chmod +x /update_mobile.sh
RUN chmod +x /update_angularjs_layout.sh

RUN apt-get update
RUN apt-get install -y build-essential g++ software-properties-common git curl wget python python2.7-dev git nginx postgresql postgresql-server-dev-all python-setuptools python-pip gunicorn supervisor

RUN curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
RUN apt-get install -y nodejs


# install bower
RUN npm install --global bower gulp
RUN /update_angularjs_layout.sh

RUN pip install -U pip setuptools
RUN pip install -r requirements_fab.txt
RUN pip install -r requirements.txt


EXPOSE 8000
EXPOSE 8080
EXPOSE 5432
EXPOSE 5001
EXPOSE 8545
EXPOSE 80

# nginx
RUN rm -v /etc/nginx/nginx.conf
ADD nginx.conf /etc/nginx/
CMD service nginx start