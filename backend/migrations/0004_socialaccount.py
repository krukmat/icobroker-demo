# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0003_auto_20160718_1940'),
    ]

    operations = [
        migrations.CreateModel(
            name='SocialAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254, null=True)),
                ('uid', models.CharField(default=b'', max_length=255, null=True)),
                ('user', models.ForeignKey(related_name='social_accounts', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
