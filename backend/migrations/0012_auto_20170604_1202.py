# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0011_rewardxuser'),
    ]

    operations = [
        migrations.AddField(
            model_name='rewardxuser',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2017, 6, 4, 12, 1, 52, 246649, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='rewardxuser',
            name='pending',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='rewardxuser',
            name='updated',
            field=models.DateTimeField(default=datetime.datetime(2017, 6, 4, 12, 2, 5, 106152, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
