from django.db import models
from django.contrib.auth.models import User, BaseUserManager
from address.models import AddressField
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.db.models import signals
from django.dispatch import receiver
from authtools.models import AbstractEmailUser
from django.utils import timezone

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.contrib.auth.tokens import default_token_generator
import urllib
from email.MIMEImage import MIMEImage

CURRENCIES = [('AR$', 'Peso Argentino'), ('US$', 'US Dollar'), ('EUR', 'Euro')]
REWARD_TYPE = [('DISCOUNT', 'Descuento'), ('VOUCHER', 'Voucher'), ('PHYSICAL', 'Fisico')]
REWARD_STATUS = [('PENDIENTE','pendiente'), ('CANCELADO','cancelado'), ('CANJEADO','canjeado'), ('VENCIDO','vencido')]


class UserManager(BaseUserManager):
    def create(self, email, password='', is_admin=False, **kwargs):
        email = self.normalize_email(email)
        user = self.model(email=email, is_admin=is_admin, is_active=True, **kwargs)
        user.set_password(password)
        user.save()
        return user

    def create_user(self, email=None, password=None, **kwargs):
        return self.create(email, password, False, **kwargs)

    def create_superuser(self, email, password, **kwargs):
        return self.create(email, password, True, **kwargs)


class Category(models.Model):
    description = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = _("Category")

    def __unicode__(self):
        return self.description


class Company(models.Model):
    id = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    category = models.ForeignKey(Category)
    address = AddressField()
    description = models.TextField(blank=True)
    website = models.URLField(blank=True)
    facebook = models.URLField(blank=True)
    twitter = models.URLField(blank=True)
    cuit = models.CharField(max_length=30)
    razon_social = models.CharField(max_length=255)
    legales = models.TextField()
    image_url = models.URLField(blank=True)
    share_reward_points = models.IntegerField(default=0)
    share_company_points = models.IntegerField(default=0)

    class Meta:
        verbose_name = _("Company")
        # The plurality here is clearly incorrect (read: "Accounts"), but this is intentional
        # as the User <-> Account relationship is strictly One-to-One; the admin panel label
        # would otherwise be confusing.
        verbose_name_plural = _("Company")
        ordering = ('user',)

    def __unicode__(self):
        return self.name


class Reward(models.Model):
    company = models.ForeignKey(Company)
    name = models.CharField(max_length=100, null=True)
    description = models.TextField(default='')
    type = models.CharField(max_length=100, choices=REWARD_TYPE, default='DISCOUNT')
    currency = models.CharField(max_length=100, choices=CURRENCIES, null=True)
    terms = models.TextField(default='')
    points = models.IntegerField(default=0)
    stock = models.IntegerField(default=0)
    period_from = models.DateTimeField(null=True)
    period_to = models.DateTimeField(null=True)
    active = models.BooleanField(default=False)
    codigo = models.CharField(max_length=50, default='', null=True)
    #Image url
    image_url = models.URLField(blank=True)

    def __str__(self):
        return self.name


class Subscription(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    points_redeemed = models.IntegerField(default=0)
    points_pending = models.IntegerField(default=0)
    share_social = models.BooleanField(default=False)
    company = models.ForeignKey(Company, related_name='user_subscriptions')
    shared_rewards = models.ManyToManyField(Reward)

    def __unicode__(self):
         if self.mypointsuser_set.all():
            return self.company.name + '-' + self.mypointsuser_set.all()[0].email
         return self.company.name

    def get_user_email(self):
        if len(self.mypointsuser_set.all())>0:
            return self.mypointsuser_set.all()[0].email
        return None

    def available_rewards(self):
        return Reward.objects.filter(company=self.company).filter(points__lte=self.points_pending)

class MyPointsUser(AbstractEmailUser):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    email_is_verified = models.BooleanField(default=False)
    is_company = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False, editable=False)
    is_super_admin = models.BooleanField(default=False, editable=False)
    # Only for end user. TODO: Buscar otra forma de hacerlo
    #points_redeemed = models.IntegerField(default=0)
    #points_pending = models.IntegerField(default=0)
    subscriptions = models.ManyToManyField(Subscription)
    first_name = models.CharField(_('first_name'), max_length=255, null=True)
    last_name = models.CharField(_('last_name'), max_length=255, null=True)
    phone = models.CharField(_('phone'), max_length=255, null=True)
    address = models.CharField(_('address'), max_length=255, null=True)
    city = models.CharField(_('city'), max_length=255, null=True)
    country = models.CharField(_('country'), max_length=255, null=True)
    state = models.CharField(_('state'), max_length=255, null=True)

    offset = models.SmallIntegerField('timezone offset in minutes', default=0)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = UserManager()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def get_short_name(self):
        return self.email

    def __unicode__(self):
        return self.email

    def profile(self):
        return self.profile

    def my_rewards(self):
        return RewardxUser.objects.filter(user=self)

    def my_company(self):
        if self.is_company:
            return Company.objects.filter(user=self)
        return None


class SocialAccount(models.Model):
    user = models.ForeignKey(MyPointsUser, related_name='social_accounts')
    email = models.EmailField(null=True)
    uid = models.CharField(max_length=255, null=True, default='')
    social = models.CharField(max_length=255, null=True, default='')


class RewardxUser(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    reward = models.ForeignKey(Reward)
    user = models.ForeignKey(MyPointsUser, related_name='rewards')
    code = models.CharField(max_length=255, null=False)
    pending = models.BooleanField(default=True)
    status = models.CharField(max_length=50, choices=REWARD_STATUS, default='PENDIENTE')
    shared_in_social = models.BooleanField(default=False)