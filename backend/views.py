# Create your views here.

from rest_framework import serializers, viewsets
from backend.models import Company, Category, Reward, MyPointsUser, SocialAccount, Subscription, RewardxUser
from rest_framework import filters
from address.models import Address
import django_filters
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
import base64
import time
import hashlib
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, RedirectView
import requests
from rest_framework.decorators import api_view


class EndUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = MyPointsUser
        fields = ('pk', 'email',
                  'first_name', 'last_name', 'address', 'city', 'country','state',
                  'phone', 'password')


class ActivateAccount(RedirectView):

    def get(self,request):
        from django.contrib.auth.tokens import default_token_generator
        uid = request.GET.get('uid')
        user = MyPointsUser.objects.get(pk=uid)
        token = request.GET.get('token')
        if default_token_generator.check_token(user, token):
            user.is_active = True
            user.save()
            user.email_welcome()
            return HttpResponseRedirect('http://mypoints.com.ar/mobileapp/#/activation')
        return HttpResponseRedirect('http://mypoints.com.ar/mobileapp/')

# Empieza lo que interesa
@api_view()
def get_coins_top_list(request):
    url = 'https://www.cryptocompare.com/api/data/toplistvolumesnapshot/?limit=ALL&symbol=btc'
    response = requests.get(url).json()
    return Response(response)

@api_view()
def get_mining_equipment(request):
    url = 'https://www.cryptocompare.com/api/data/miningequipment/'
    response = requests.get(url).json()
    return Response(response)

@api_view()
def get_what_to_mine(request):
    url = 'http://whattomine.com/coins.json'
    response = requests.get(url).json()
    return Response(response)