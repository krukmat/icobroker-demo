angular.module('angularApp.controllers', ['angularApp.services', 'ngToast', 'ja.qr', 'ui.grid'])
.controller('HomeCtrl', function($window, $http, $scope, $resource, CryptoCompareAPI, WhatToMineAPI, AuthService, $location, uiGridConstants, $timeout) {
   $scope.indexClass = 'home';
   $scope.coins = [];
   $scope.max_values = [];
   $scope.min_values = [];
   $scope.refresh = true;
   $scope.columns = [{ field: 'coin' }, { field: 'min_value' }, { field: 'max_value' }, { field: 'average' }, { field: 'deviation' }];
   $scope.gridOptions = {
    enableSorting: true,
    enableRowHashing:false,
    columnDefs: $scope.columns
   };
   var refresh = function() {
    $scope.refresh = true;
    $timeout(function() {
      $scope.refresh = false;
    }, 0);
  };
   $scope.gridOptions.data = [];
   function findCoin(coin){
        var result = $.grep($scope.gridOptions.data, function(e){ return e.coin == coin; });
        if (result.length == 0) {
          return null;
        } else if (result.length == 1) {
          return result[0];
        } else {
          return result[0];
          console.log('multiples elementos');
        }
   }

   $scope.$on('max-value-changed', function(event, args) {
        var symbol = args.symbol;
        var value = args.value;
        var market = args.market;
        //console.log('MAX: ',symbol, value);
        //$scope.max_values[symbol] = value;
        var coinData = findCoin(symbol);
        coinData['max_value'] = value + '(' + market + ')';
        //$scope.gridOptions.columnDefs.splice(0, $scope.gridOptions.columnDefs.length);
        $scope.$apply();
        // do what you want to do
    });

    $scope.$on('new-price', function(event, args) {
        symbol = args.symbol;
        market = args.market;
        var element = findCoin(symbol);
        element.deviation = $scope.standard_deviation[symbol];
        element.average = $scope.average_values[symbol];
        //$scope.operations[symbol][market] = $scope.operations[symbol][market] + 1;
        //$scope.gridOptions.columnDefs.splice(0, $scope.gridOptions.columnDefs.length);
        $scope.$apply();
    });

    $scope.$on('min-value-changed', function(event, args) {
        var symbol = args.symbol;
        var value = args.value;
        var market = args.market;
        //console.log('MIN: ',symbol, value);
        //$scope.min_values[symbol] = value;
        var coinData = findCoin(symbol);
        coinData['min_value'] = value + '(' + market + ')';
        //$scope.gridOptions.columnDefs.splice(0, $scope.gridOptions.columnDefs.length);
        $scope.$apply()
        // do what you want to do
    });

    $scope.$on('remove-coin', function(event, args) {
        var coin = args.coin;
        console.log('DELETE: ',coin);
        var element = findCoin(coin);
        var index = $scope.gridOptions.data.indexOf(element);
        if (index > -1) {
            //$scope.coin_symbols = [];
            $scope.gridOptions.data.splice(index, 1);
            $scope.$apply();
        }
        //$scope.gridOptions.columnDefs.splice(0, $scope.gridOptions.columnDefs.length);
        //$scope.min_values[symbol] = value;
        //
        // do what you want to do
    });
   $scope.getOperations = function(coin, market){
        return $window.operations[coin][market];
   }
   CryptoCompareAPI.getTopCurrencies().then(function(proxy){
       var list = proxy.data.Data;

       // Los primeros 300 elementos
       $scope.current_values = {};
       $scope.standard_deviation = {};
       $scope.coin_symbols = [];
       $scope.average_values = [];
       $window.operations = {};
       for (var element=0;element<100; element++){
            var coin = list[element].split('~');

            $scope.coins.push(coin);
            $scope.current_values[coin[2]] = {};
            $scope.max_values[coin[2]] = {};
            $scope.min_values[coin[2]] = {};

            $scope.standard_deviation[coin[2]] = 0;
            $scope.average_values[coin[2]] = 0;
            $scope.max_values[coin[2]].value = 0;
            $window.operations[coin[2]] = {};
            $scope.min_values[coin[2]].value = 100;
            $scope.coin_symbols.push(coin[2]);

            $scope.gridOptions.data.push({coin: coin[2]});
            CryptoCompareAPI.subscribeToCoinMarkets(coin[2], $scope);
       }
       refresh();
       //{SubscriptionId}~{ExchangeName}~{FromSymbol}~{ToSymbol}...

       //CryptoCompareAPI.getEquipmentData();
       // filtrar el listado a los primeros 20, aunque podria ser configurable. diferencia por % de volumen manejado en las ultimas 24hs.
       // Necesito en nombre de la moneda. Buscar de que listado la puedo sacar para WhatToMineAPI.getStats();
       // podria agregarse parametro de filtro por valor en BTC (para buscar rango de monedas).
       // TODO: El usuario configure monedas o rango del valor del precio que le interesar monitorear.
       // split del texto


   });

})
.controller('UserCtrl', function ($rootScope, $scope, $http, $window, $location, AuthService) {
      $scope.user = {email: '', password: ''};
      $scope.isAuthenticated = false;
      $scope.isRegister = false;
      $scope.welcome = '';
      $scope.message = '';

      $rootScope.$on('event:social-sign-in-success', function(event, userDetails){
          console.log(userDetails);
      });

      $scope.selected = 'None';
      $scope.menuOptions = [
          ['Mi cuenta', function ($itemScope) {
              $location.path("/accounts");
          }],
          ['Actualizar Contraseña', function ($itemScope) {
              $location.path("/password");
          }],
          ['Salir', function ($itemScope) {
              $scope.logout();
              $scope.indexClass = '';
              $location.path('/login');
          }]
      ];

      $scope.has_social = function(social) {
          var social_exists = false;
          if ($scope.profile) {
            console.log($scope.profile.social_accounts);
            $scope.profile.social_accounts.forEach(function(social_network) {
                  if (social_network.social === social) {
                      social_exists = true;
                  }
            });
            return social_exists;
          }
          return true;
      }

      $scope.login = function () {
          AuthService.login($scope.user)
          .then(function (data) {
            $window.sessionStorage.token = data.data.auth_token;
            $scope.isAuthenticated = true;
            $scope.indexClass = 'home';
            $location.path("/");
            location.reload();
          }, function (data) {
            // Erase the token if the user fails to log in
            // delete $window.sessionStorage.token;
            $scope.isAuthenticated = false;
            $scope.indexClass = '';

            // Handle login errors here
            $scope.error = data.data.non_field_errors[0];
            $scope.welcome = '';
          });
      };

      $scope.register = function () {
          AuthService.register($scope.user)
          .then(function (data, status, headers, config) {
            $location.path("/login");
          }, function (data) {
            // Erase the token if the user fails to log in
            delete $window.sessionStorage.token;
            $scope.isAuthenticated = false;

            // Handle login errors here
            $scope.error = 'Error: Invalid user or password';
            $scope.welcome = '';
          });
      };

      $scope.logout = function () {
        $scope.welcome = '';
        $scope.message = '';
        $scope.isAuthenticated = false;
        delete $window.sessionStorage.token;
      };

      $scope.callRestricted = function () {
        AuthService.me()
        .then(function (data) {
          $scope.isAuthenticated = true;
          $scope.profile = data.data;
          $scope.indexClass = 'home';
        }, function (data) {
          $scope.isAuthenticated = false;
          $scope.indexClass = '';
          $location.path("/login");
        });
      };
      $scope.callRestricted()

    })
.controller('AccountCtrl', function($scope, AuthService, ngToast) {
  $scope.update = function update(){
      AuthService.update_me($scope.profile);
      ngToast.create({
        className: 'success',
        content: 'Updated',
        dismissButton: true
      });
  }
})
.controller('PasswordCtrl', function($scope, AuthService, ngToast) {
  $scope.passwords = {};
  $scope.passwords.new_password = '';

  $scope.update = function update(){
      AuthService.update_password($scope.passwords);
      ngToast.create({
        className: 'success',
        content: 'Password Updated',
        dismissButton: true
      });
  }
});
